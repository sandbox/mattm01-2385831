;(function ($) {
  Drupal.behaviors.linkitTweaksAddLinkItMailtoField = {
    attach: function (context, settings) {
      $('.js-mailto-insert').once().click(function(event) {
        // Setting up some stuff
        var emailPattern = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"),
            editor = CKEDITOR.instances[Drupal.settings.linkit.currentInstance.source],
            selectedText = editor.getSelection();
        
        // Going to be used later
        var link = null,
            linkText = null,
            linkHref = null;
            
        // Form values
        var emailAddress = $('.form-item-email input', context).val(),
            subject = $('.form-item-subject input', context).val(),
            body = $('.form-item-body textarea', context).val();
            
        $('.validation-message', context)
          .removeClass('messages error')
          .text('');

        if (emailPattern.test(emailAddress)) {
          link = editor.document.createElement('a');
          subject = encodeURIComponent(subject);
          body = encodeURIComponent(body);

          href = "mailto:" + emailAddress;

          if (subject) {
            href += "?subject=" + subject;
          }

          if (body) {
            href += "&body=" + body;
          }

          link.setAttribute('href', href);
          linkText = selectedText.getSelectedText() || emailAddress;
          link.setHtml(linkText);

          editor.insertElement(link);
          
          $(this).siblings('#linkit-cancel').click();
        } else {
          $(this).siblings('.validation-message')
            .addClass('messages error')
            .text('Please enter a valid email.');          
        }
      });
    }
  };
})(jQuery);