;(function ($) {
  var anchorFieldMarkup = '<div class="anchor-field-wrapper">' +
                            '<label for="anchor-field">Page Anchor</label>' +
                            '<div class="form-item">' +
                              '<input type="text" id="anchor-field" name="anchor-field" class="js-anchor-field form-text">' +
                            '</div>' +
                          '</div>',
      insertButtonMarkup = '<a class="js-internal-link-insert button">Insert Link</a>',
      cancelButtonMarkup = '<a id="linkit-cancel" class="linkit-cancel-processed">Cancel</a>';

  Drupal.behaviors.linkitTweaksAddAnchorToLinkitPicker = {
    attach: function (context, settings) {
      $('.linkit-tweaks-view-container .view').once().append('<div class="linkit-tweaks-fields">' + anchorFieldMarkup + insertButtonMarkup + cancelButtonMarkup + '</div>');  

      $('.js-anchor-insert', context).once().click(function (event) {
        // Setting up some stuff
        var editor = CKEDITOR.instances[Drupal.settings.linkit.currentInstance.source],
            selectedText = editor.getSelection();
        
        // Going to be used later
        var link = null,
            linkText = null;
            
        // Form values
        var anchor = $('.form-item-anchor-link input', context).val();

        if (anchor && anchor !== "") {
          anchor = "#" + anchor;
        }
            
        link = editor.document.createElement('a');

        link.setAttribute('href', anchor);
        linkText = selectedText.getSelectedText() || anchor;
        link.setHtml(linkText);

        editor.insertElement(link);
        
        $(this).siblings('#linkit-cancel').click();
      });

      $('.js-internal-link-insert', context).once().click(function (event) {
        var path = $('#linkit-dashboard-form .linkit-path-element').val(),
            anchor = $('.js-anchor-field', context).val();

            if (anchor && anchor !== "") {
              anchor = "#" + anchor;
            }

        $('#linkit-dashboard-form .linkit-path-element').val(path + anchor);
        $('#edit-linkit-insert').click();
      });
    }
  };
})(jQuery);