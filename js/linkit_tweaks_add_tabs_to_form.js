;(function ($) {
  Drupal.behaviors.linkitTweaksAddTabsToForm = {
    attach: function (context, settings) {
      // Hack to get the linkit modal to resize after it has fully rendered
      // CKEDITOR.on('dialogDefinition', function() {}); didn't work -
      // dialogDefinition event doesn't seem to be fired.
        setTimeout(function () {
          $(window).trigger('resize');
        }, 150);  

      // Move Linkit Picker to its tab with JS since doing it 
      // server side was breaking the exposed filters.
        if ($('.linkit-picker-node-wrapper').length) {
          $('.view-linkit-picker-linkit_picker_node', context)
            .appendTo('.linkit-picker-node-wrapper', context)
            .show();
        }

        if ($('.linkit-picker-user-wrapper').length) {
          $('.view-linkit-picker-linkit_picker_user', context)
            .appendTo('.linkit-picker-user-wrapper', context)
            .show();
        }

        $('.linkit-picker-container').not('.linkit-tweaks-view-container').remove();
        
        $('#linkit-modal').trigger('linkit-tweaks-linkit-picker-moved');
    }
  };
})(jQuery);