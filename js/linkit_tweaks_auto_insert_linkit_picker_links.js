;(function ($) {
  Drupal.behaviors.linkitTweaksAutoInsertLinkitPickerLinks = {
    attach: function (context, settings) {
      $('.view-linkit-picker-node .views-field-title.linkit-row-processed').once().click(function(event) {
        $('#edit-linkit-insert').click();
      });
    }
  };
})(jQuery);