;(function ($) {
  Drupal.behaviors.linkitTweaksHighlightSelectedRowInLinkitPicker = {
    attach: function (context, settings) {
      $('.linkit-tweaks-view-container .view tbody tr').once().click(function(event) {
        $('.linkit-tweaks-view-container .view tbody tr.selected').removeClass('selected');
        $(this).addClass('selected');
      });
    }
  };
})(jQuery);