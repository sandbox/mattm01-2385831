<?php
/**
 * @file
 * Contains the callback function that builds the admin form.
 */

/**
 * Page callback.
 */
function linkit_tweaks_admin_settings_form($form, $form_state) {
  $form = array(
    'linkit_tweaks_change_linkit_icon' => array(
      '#type' => 'checkbox',
      '#title' => t('Change LinkIt WYSIWYG Icon?'),
      '#default_value' => variable_get('linkit_tweaks_change_linkit_icon', 0),
      '#description' => t("Check this box if you want to change the LinkIt WYSIWYG icon."),
    ),

    'linkit_tweaks_add_tabs_to_form' => array(
      '#type' => 'checkbox',
      '#title' => t('Add Vertical Tabs to LinkIt Form?'),
      '#default_value' => variable_get('linkit_tweaks_add_tabs_to_form', 0),
      '#description' => t("Check this box if you want to add vertical tabs to the LinkIt form to better group items."),
    ),

    'linkit_tweaks_change_form_description' => array(
      '#type' => 'checkbox',
      '#title' => t('Change Description?'),
      '#default_value' => variable_get('linkit_tweaks_change_form_description', 0),
      '#description' => t("Check this box if you want to change the description text."),
    ),

    'linkit_tweaks_hide_linkit_search' => array(
      '#type' => 'checkbox',
      '#title' => t('Hide LinkIt Search?'),
      '#default_value' => variable_get('linkit_tweaks_hide_linkit_search', 0),
      '#description' => t("Check this box if you want to hide the Linkit it search form. This is helpful if you are using Linkit Picker and don't want two search fields."),
    ),

    'linkit_tweaks_highlight_selected_row_in_linkit_picker' => array(
      '#type' => 'checkbox',
      '#title' => t('Highlight Selected Row in LinkIt Picker?'),
      '#default_value' => variable_get('linkit_tweaks_highlight_selected_row_in_linkit_picker', 0),
      '#description' => t("Check this box if you want to highlight the selected row in Linkit Picker. E.g. when you click a row, change the styling."),
    ),

    'linkit_tweaks_auto_insert_linkit_picker_links' => array(
      '#type' => 'checkbox',
      '#title' => t('Auto Insert Links From LinkIt Picker?'),
      '#default_value' => variable_get('linkit_tweaks_auto_insert_linkit_picker_links', 0),
      '#description' => t("Check this box if you want to automatically insert links when they are clicked in LinkIt picker. Normally a user has to click the link, then click insert. <strong>Auto Insert Links and Add Anchor Field don't really work together.</strong>"),
    ),

    'linkit_tweaks_add_anchor_fields' => array(
      '#type' => 'checkbox',
      '#title' => t('Add Anchor Field to LinkIt Picker?'),
      '#default_value' => variable_get('linkit_tweaks_add_anchor_fields', 0),
      '#description' => t("Check this box if you want to add an anchor tab and an anchor field to the Linkit Picker view. <strong>Works best when tabs are added to the form. Also, Auto Insert Links and Add Anchor Field don't really work together.</strong>"),
    ),

    'linkit_tweaks_add_mailto_field' => array(
      '#type' => 'checkbox',
      '#title' => t('Add Mailto Fields to Linkit?'),
      '#default_value' => variable_get('linkit_tweaks_add_mailto_field', 0),
      '#description' => t("Check this box if you want to add fields to create a mailto link in the linkit dashboard. <strong>Works best when tabs are added to the form.</strong>"),
    ),
  );

  return system_settings_form($form);
}
